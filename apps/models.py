from django.db import models
from datetime import datetime
from django.contrib.postgres.fields import ArrayField
from django.utils.text import slugify
# Create your models here.

class User(models.Model):
    job = models.CharField(max_length=255)
    birthday = models.DateField(auto_created=True)
    website = models.URLField(max_length=255)
    phone = models.CharField(max_length=18)
    city = models.CharField(max_length=255)
    degree = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    freelance = models.BooleanField(default=False)
    types = ArrayField(models.CharField(max_length=255), blank=True, null=True)
    about = models.TextField()
    image = models.ImageField(upload_to='static/profile/')


    @property
    def age(self):
        today = datetime.today()
        return today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))


class Message(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    subject = models.CharField(max_length=255)
    message = models.TextField()


class Category(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
            while Project.objects.filter(slug=self.slug).exists():
                slug = Project.objects.filter(slug=self.slug).first().slug
                if '-' in slug:
                    try:
                        if slug.split('-')[-1] in self.title:
                            self.slug += '-1'
                        else:
                            self.slug = '-'.join(slug.split('-')[:-1]) + '-' + str(int(slug.split('-')[-1]) + 1)
                    except:
                        self.slug = slug + '-1'
                else:
                    self.slug += '-1'
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class Project(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    project_date = models.CharField(max_length=255)
    url = models.URLField(max_length=255)
    client = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    category = models.ForeignKey(Category, models.PROTECT)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
            while Project.objects.filter(slug=self.slug).exists():
                slug = Project.objects.filter(slug=self.slug).first().slug
                if '-' in slug:
                    try:
                        if slug.split('-')[-1] in self.title:
                            self.slug += '-1'
                        else:
                            self.slug = '-'.join(slug.split('-')[:-1]) + '-' + str(int(slug.split('-')[-1]) + 1)
                    except:
                        self.slug = slug + '-1'
                else:
                    self.slug += '-1'
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class ImageList(models.Model):
    project = models.ForeignKey(Project, models.PROTECT, related_name='photos')
    image = models.ImageField(upload_to='photos/')


class Skill(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name