from django.shortcuts import render
from django.urls import reverse_lazy

from django.views.generic import FormView, DetailView, ListView
from apps.forms import MessageForm
from apps.models import Project, Skill, Category, User


class MessageView(FormView, ListView):
    template_name = 'index.html'
    form_class = MessageForm
    success_url = reverse_lazy('index')
    queryset = Project.objects.all()
    context_object_name = 'projects'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = User.objects.all()
        context['skills'] = Skill.objects.all()
        context['categories'] = Category.objects.all()
        # print(User.objects.all().users)
        return context

# def message_view(requset):
#
#
#     user = User.objects.all()
#     skills = Skill.objects.all()
#     category = Category.objects.all()
#     project = Project.objects.all()
#
#     ctx = {
#         'users': user,
#         'skills': skills,
#         'categories': category,
#         'projects': project
#     }
#     print((ctx))
#     print(user, skills)
#     return render(requset, 'index.html', ctx)
class CustomDetailView(DetailView):
    template_name = 'portfolio-details.html'
    queryset = Project.objects.all()
    context_object_name = 'project'
