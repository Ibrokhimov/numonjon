from django.contrib import admin
from .models import User, Message, Category, Project, ImageList, Skill
# Register your models here.

class ImageAdmin(admin.StackedInline):
    model = ImageList



class ProjectAdmin(admin.ModelAdmin):
    inlines = [ImageAdmin]
    exclude = ('slug', 'image')


class CategoryAdmin(admin.ModelAdmin):
    exclude = ('slug',)

admin.site.register(User)
admin.site.register(Message)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Skill)